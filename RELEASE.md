# RELEASE NOTES

## 0.1.0 (2017-11-30)

- created initial MJTA logo
- manually created using svg format

## 0.2.0 (2021-04-10)

- major update and improvement on the logo
- removed deprecated SVG attributes
- added padding
- redrawn SVG canvas
- registered and inserted copyright files from copyrighted.com
- simplified, unified, and aligned repository standards
- created README.md, RELEASE.md, and LICENSE files

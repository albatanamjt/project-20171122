# README

- Designer: Michael James T Albatana
- email: albatanamjt.dev@icloud.com

## OBJECTIVE

- create personal logo

## FEATURES

- digital art using script coordinates, SVG Canvas
- lines showcasing the initials of the logo designer "MJTA"
- M (main canvas), J (rightmost), T (cross-like lines on right), A (pattern on middle)
- straight lines, right angle

## COPYRIGHTS

- Registered, Protected & Monitored by Copyrighted.com © 2021
